import csv
from datetime import datetime
import serial
import time
import os

# Funcție pentru a verifica dacă fișierul există și pentru a crea fișierul dacă nu există
def check_file_existence(file_path):
    if not os.path.exists(file_path):
        with open(file_path, mode='w', newline='') as file:
            writer = csv.writer(file)
            writer.writerow(['Timestamp', 'Temperature (C)', 'Humidity (%)', 'pozitieClapeta (grade)', 'Ventilator'])
        print(f"Fisierul {file_path} a fost creat cu succes.")

# Funcție pentru a scrie datele în fișierul CSV
def write_to_csv(file_path, timestamp, temperature, humidity, pozitieClapete, ventilatorStatus):
    with open(file_path, mode='a', newline='') as file:
        writer = csv.writer(file)
        writer.writerow([timestamp, temperature, humidity, pozitieClapete, ventilatorStatus])

# Funcție pentru a prelua datele de la senzorul conectat la Arduino
def read_sensor_data(ser):
    try:
        if ser.in_waiting > 0:
            line = ser.readline().decode('utf-8').strip()
           # print(f"Linie citită de la serial: {line}")  # Adăugat pentru debug
            parts = line.split(',')
            if len(parts) == 4:
                try:
                    temperature = float(parts[0])
                    humidity = float(parts[1])
                    pozitieClapete = int(parts[2])
                    ventilatorStatus = parts[3]
                    timestamp = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                    return timestamp, temperature, humidity, pozitieClapete, ventilatorStatus
                except ValueError as e:
                    print(f"ValueError: {e} - Date invalide: {parts}")  # Adăugat pentru debug
            else:
                print(f"Format de date invalid: {parts}")
    except serial.SerialException as e:
        print(f"SerialException: {e}")
    except UnicodeDecodeError as e:
        print(f"UnicodeDecodeError: {e}")
    return None, None, None, None, None

# Configurarea portului serial pentru Arduino
ser = serial.Serial('COM5', 9600, timeout=1)

# Verificare existență fișier CSV și creare dacă nu există
file_path = 'e:/licenta/Python source/date_sera.csv'
check_file_existence(file_path)

# Buclă pentru a prelua datele de la senzor și a le scrie în fișierul CSV
while True:
    # Preluăm datele de la senzor
    sensor_data = read_sensor_data(ser)
    if sensor_data:
        timestamp, temperature, humidity, pozitieClapete, ventilatorStatus = sensor_data

        # Scriem datele în fișierul CSV
        write_to_csv(file_path, timestamp, temperature, humidity, pozitieClapete, ventilatorStatus)

        # Afișăm datele preluate pentru verificare
        print(f'Date preluate: {timestamp}, Temperatura: {temperature}°C, Umiditate: {humidity}%, Pozitia Clapete: {pozitieClapete}, Ventilator: {ventilatorStatus}')

    # Așteptăm 10 secunde înainte de a prelua următoarele date
    time.sleep(10)
