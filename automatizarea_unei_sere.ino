#include <DHT.h>
#include <Servo.h>
#include <LiquidCrystal.h>

#define DHTPIN 2
#define DHTTYPE DHT11
DHT dht(DHTPIN, DHTTYPE);
LiquidCrystal lcd(13, 12, 11, 10, 9, 8); // Inițializarea modulului LCD cu pini RS, E, D4, D5, D6, D7
Servo servoClapete;

const int ledRosuPin = 5;
const int ledGalbenPin = 6;
const int ledVerdePin = 4;
const int pinTensiune = 3; // Pinul pentru tensiunea de 5V

int pozitieClapete = 0; // Starea inițială a clapetelor (0 închise, 90 deschise)
bool ventilatorPornit = false; // Starea ventilatorului (pornit/oprit)

void setup() {
    dht.begin();
    Serial.begin(9600); // Inițializarea comunicării seriale cu o viteză de baud de 9600

    pinMode(ledRosuPin, OUTPUT);
    pinMode(ledGalbenPin, OUTPUT);
    pinMode(ledVerdePin, OUTPUT);
    pinMode(pinTensiune, OUTPUT); // Configurarea pinului pentru tensiune ca ieșire

    servoClapete.attach(7); // Conectarea servomotorului la pinul 7
    servoClapete.write(pozitieClapete); // Închiderea completă a clapetelor
    digitalWrite(ledVerdePin, HIGH);
    digitalWrite(ledRosuPin, LOW);
    digitalWrite(ledGalbenPin, LOW); // Inițializarea ledului galben ca stins

    // Inițializarea modulului LCD
    lcd.begin(16, 2); // Configurarea afișajului LCD cu 16 caractere pe 2 linii
    lcd.print("Buna!");
    delay(2000); // Așteptarea 2 secunde pentru a afișa mesajul inițial
    lcd.clear(); // Ștergerea mesajului inițial
}

void loop() {
    lcd.setCursor(0, 0); // Setarea cursorului la începutul primei linii
    
    float temperature = dht.readTemperature(); // Citirea temperaturii de la senzor
    float humidity = dht.readHumidity(); // Citirea umidității de la senzor

    if (isnan(temperature) || isnan(humidity)) {
        Serial.println("Eroare la citirea datelor de la DHT!"); // Afisarea mesajului de eroare în cazul unei citiri incorecte
        return;
    }

    // Controlul tensiunii pe pinul 3 (pinTensiune) în funcție de temperatură
    if (temperature > 28) {
        digitalWrite(pinTensiune, HIGH); // Pornirea tensiunii de 5V și a ventilatorului
        ventilatorPornit = true;
    } else if (temperature < 27.5) {
        digitalWrite(pinTensiune, LOW); // Oprirea tensiunii de 5V și a ventilatorului
        ventilatorPornit = false;
    }

    // Controlul clapetelor și LED-urilor în funcție de temperatură
    if (temperature < 27.5) {
        pozitieClapete = 0; // Închiderea clapetelor complet
        digitalWrite(ledGalbenPin, LOW);
        digitalWrite(ledRosuPin, LOW);
        digitalWrite(ledVerdePin, HIGH);
    } else if (temperature >= 27.5 && temperature <= 28) {
        pozitieClapete = 90; // Deschiderea clapetelor la 90 de grade
        digitalWrite(ledVerdePin, LOW);
        digitalWrite(ledRosuPin, LOW);
        digitalWrite(ledGalbenPin, HIGH);
    } else {
        pozitieClapete = 90; // Păstrează clapetele deschise
        digitalWrite(ledGalbenPin, LOW); // Aprinde ledul galben
        digitalWrite(ledVerdePin, LOW);
        digitalWrite(ledRosuPin, HIGH); // Aprinde ledul roșu
    }
    servoClapete.write(pozitieClapete);

    // Trimiterea datelor către serial
    Serial.print(temperature);
    Serial.print(",");
    Serial.print(humidity);
    Serial.print(",");
    Serial.print(pozitieClapete);
    Serial.print(",");
    Serial.println(ventilatorPornit ? "ON" : "OFF");

    // Afișare date pe LCD
    lcd.print("Temp: ");
    lcd.print(temperature, 1); // Afișarea temperaturii cu o zecimală
    lcd.print("C ");

    lcd.setCursor(0, 1); // Setarea cursorului la începutul celei de-a doua linii
    lcd.print("Hum: ");
    lcd.print(humidity, 1); // Afișarea umidității cu o zecimală
    lcd.print("%");

    delay(2000); // Așteaptă 2 secunde înainte de a repeta loop-ul
}
